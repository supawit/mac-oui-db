simple mac addres oui lookup api.

use database from https://macaddress.io/database-download/csv.

* deployment
```
$git clone https://gitlab.com/supawit/mac-oui-db.git
$cd mac-oui-db
$touch ngx/htpasswd.local // then edit this file add your username and password for nginx basic authentication
$docker-compose up -d
```

* using api
```
$curl -q -u username:password http://api-hostname-or-ip-address:8085/a8-9c-ed-00-00-00
```

* output
```
{
  "oui": "A8:9C:ED",
  "isPrivate": "0",
  "companyName": "Xiaomi Communications Co Ltd",
  "companyAddress": "The Rainbow City of China Resources NO.68, Qinghe Middle Street Haidian District, Beijing 100085 CN",
  "countryCode": "CN",
  "assignmentBlockSize": "MA-L",
  "dateCreated": "2018-12-04",
  "dateUpdated": "2018-12-04"
}
```
or
```
{
  "error": true,
  "detail": "Not Found"
}
```