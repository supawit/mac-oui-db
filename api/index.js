const csvFilePath = './macaddress.io-db.csv'
const csv = require('csvtojson')
const express = require('express')
const cors = require('cors')


//function
let macDb = []
const loadMacDb = async () => {
    macDb = await csv().fromFile(csvFilePath)
}
loadMacDb()

const findMacVendor = async (req,res,next) => {
    const mac = req.params.macaddress
    const macaddress = mac.replace(/\-/g,':').toUpperCase()
    
    const result = macDb.find((obj)=>{
        const ouiLength = obj.oui.length
        const macaddressOui = macaddress.substring(0,ouiLength)
        //console.log(macaddress,obj.oui,ouiLength, macaddressOui);
        return obj.oui === macaddressOui                
    })

    //console.log(result);
    if (result) {
        res.status(200).send(result)
    } else {
        req.httpStatusCode = 404
        next(
            {
                error: true,
                detail: "Not Found"
            }
        )        
    }       
}

const errorHandler = (error,req,res,next) => {
    const httpStatusCode = req.httpStatusCode?req.httpStatusCode:500    
    res.status(httpStatusCode).send(error)
}

const validateMacaddress = (req,res,next) => {
    const macRegex = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/
    if (!macRegex.test(req.params.macaddress)) {
        req.httpStatusCode = 400
        next(
            {
                msg: 'MAC Address format should be 00-11-22-33-44-55 or 00:11:22:33:44:55'
            }
        )
    }else{
        next()
    }
}

//express endpoint
const app = express()
app.use(cors())

app.options('*', cors())

app.get("/:macaddress",validateMacaddress,findMacVendor)

app.use(errorHandler)

app.listen(8080,'0.0.0.0')
console.log("MAC OUI API is listening on port 8080")